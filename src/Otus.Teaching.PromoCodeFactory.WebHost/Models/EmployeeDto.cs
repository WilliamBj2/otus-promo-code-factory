﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeDto
    {
        /// <example>"Жотаро"</example>
        public string FirstName { get; set; }

        /// <example>"Токийский"</example>
        public string LastName { get; set; }

        /// <example>"gigachad@rambler.ru"</example>
        public string Email { get; set; }

        /// <example>["b0ae7aac-5493-45cd-ad16-87426a5e7665"]</example>
        public List<Guid> RoleIds { get; set; }

        /// <example>25</example>
        public int? AppliedPromocodesCount { get; set; }
    }
}
