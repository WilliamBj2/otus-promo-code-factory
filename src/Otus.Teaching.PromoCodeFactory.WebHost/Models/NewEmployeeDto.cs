﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class NewEmployeeDto
    {
        /// <example>"Джейк"</example>
        [Required]
        public string FirstName { get; set; }

        /// <example>"Сулли"</example>
        [Required]
        public string LastName { get; set; }

        /// <example>"pandora@mail.ru"</example>
        [Required]
        public string Email { get; set; }

        /// <example>["53729686-a368-4eeb-8bfa-cc69b6050d02"]</example>
        [Required]
        public List<Guid> RoleIds { get; set; }

        /// <example>15</example>
        [Required]
        public int AppliedPromocodesCount { get; set; }
    }
}
