﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Зарегистрировать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<Guid> AddEmployeeAsync([FromBody] NewEmployeeDto dto)
        {
            var newEmployee = new Employee()
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
                Roles = (await _roleRepository.GetAllAsync()).Where(r => dto.RoleIds.Contains(r.Id)).ToList(),
                AppliedPromocodesCount = dto.AppliedPromocodesCount
            };

            return await _employeeRepository.AddAsync(newEmployee);
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee is null)
            {
                return NotFound();
            }

            return new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            return employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                })
                .ToList();
        }

        /// <summary>
        /// Изменить данные о сотруднике
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployeeAsync([FromQuery] Guid Id, [FromBody] EmployeeDto dto)
        {
            var employee = await _employeeRepository.GetByIdAsync(Id);
            if (employee != null)
            {
                employee.FirstName = dto.FirstName ?? employee.FirstName;
                employee.LastName = dto.LastName ?? employee.LastName;
                employee.Email = dto.Email ?? employee.Email;

                if (dto.RoleIds != null)
                {
                    employee.Roles = (await _roleRepository.GetAllAsync()).Where(r => dto.RoleIds.Contains(r.Id)).ToList();
                }

                employee.AppliedPromocodesCount =
                    dto.AppliedPromocodesCount ?? employee.AppliedPromocodesCount;

                if (await _employeeRepository.UpdateAsync(Id, employee))
                {
                    return Ok();
                }
            }

            return BadRequest();
        }

        /// <summary>
        /// Удалить информацию о сотруднике
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> RemoveEmployeeAsync([FromQuery, Required] Guid id)
        {
            if (await _employeeRepository.RemoveAsync(id))
            {
                return Ok();
            }

            return BadRequest();
        }
    }
}