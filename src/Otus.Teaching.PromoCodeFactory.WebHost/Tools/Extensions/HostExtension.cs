﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Tools.Extensions
{
    public static class HostExtension
    {
        /// <summary>
        /// Проводим миграцию
        /// </summary>
        public static async Task PreparationDatabaseAsync(this IHost host)
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;
            using var context = services.GetRequiredService<AppDbContext>();

            try
            {
                await context.Database.EnsureDeletedAsync();
                await context.Database.MigrateAsync();

                context.Set<Employee>().AddRange(FakeDataFactory.Employees);
                await context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
