﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Base;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : IBaseEntity<T>
    {
        private readonly AppDbContext _context;
        private readonly DbSet<T> _dbSet;

        public EfRepository(AppDbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        public async Task<Guid> AddAsync(T newItem)
        {
            _dbSet.Add(newItem);
            await _context.SaveChangesAsync();
            return newItem.Id;
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return _dbSet.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<bool> UpdateAsync(Guid id, T updItem)
        {
            var item = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            if (item != null)
            {
                item.Updating(updItem);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }

        public async Task<bool> RemoveAsync(Guid id)
        {
            var item = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            if (item != null)
            {
                _dbSet.Remove(item);
                await _context.SaveChangesAsync();

                return true;
            }

            return false;
        }
    }
}
