﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Base;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : IBaseEntity<T>
    {
        private static readonly object syncRoot = new object();

        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<Guid> AddAsync(T newItem)
        {
            lock (syncRoot)
            {
                newItem.Id = Guid.NewGuid();

                var list = Data as List<T>;
                list.Add(newItem);
                return Task.FromResult(newItem.Id);
            }
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<bool> UpdateAsync(Guid id, T updItem)
        {
            lock (syncRoot)
            {
                var list = Data as List<T>;
                var item = list.FirstOrDefault(i => i.Id == id);

                if (item != null)
                {
                    item.Updating(updItem);
                    return Task.FromResult(true);
                }
                return Task.FromResult(false);
            }
        }

        public Task<bool> RemoveAsync(Guid id)
        {
            lock (syncRoot)
            {
                var list = Data as List<T>;
                var item = list.FirstOrDefault(i => i.Id == id);

                return Task.FromResult(
                    !(item is null) && list.Remove(item));
            }
        }
    }
}