﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Base;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : IBaseEntity<Employee>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public List<Role> Roles { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public override Employee Updating(Employee updItem)
        {
            FirstName = updItem.FirstName;
            LastName = updItem.LastName;
            Email = updItem.Email;
            Roles = updItem.Roles;
            AppliedPromocodesCount = updItem.AppliedPromocodesCount;

            return this;
        }
    }
}