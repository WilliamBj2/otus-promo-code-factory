﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Base;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role : IBaseEntity<Role>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public override Role Updating(Role updItem)
        {
            Name = updItem.Name;
            Description = updItem.Description;

            return this;
        }
    }
}