﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Base
{
    public abstract class IBaseEntity<T> : BaseEntity where T : BaseEntity
    {
        public abstract T Updating(T updItem);
    }
}
